#include <iostream>
#include <string>
#include <chrono>
#include <list>

#include "catch.hpp"

using namespace std;

struct Length
{
    double value;

    explicit Length(double v) : value{v}
    {}
};

namespace UnsafeCode
{
    void move_vehicle(double x)
    {
        cout << "Unsafe move(" << x << ")" << endl;
    }
}

void move_vehicle(Length l)
{    
    cout << "Unsafe move(" << l.value << ")" << endl;
}

Length operator ""_cm(unsigned long long l)
{
    return Length{l * 0.1};
} 

Length operator ""_cm(long double l)
{
    return Length{l * 0.01};
} 

TEST_CASE("using custom literals")
{
    UnsafeCode::move_vehicle(100.0);

    move_vehicle(3_cm);
    move_vehicle(3.14_cm);
}

TEST_CASE("auto type deduction")
{
    auto x = 10; // int
    auto pi = 3.14; // double
    auto fpi = 3.14f; // float
    auto index = 42u; // unsigned int
    auto text = "text"; // const char*
    auto str = "text"s; // string

    auto time = 100ms; // std::chrono::milliseconds
    auto length = 100_cm; // Length

    const auto cpi = 3.14; // const double
    auto d = cpi; // double
    auto* const cptr  = &pi; // const double* const
    auto* ptr = &pi; // const double* const
}

template <typename T>
void type_deduction(T obj)
{
}

TEST_CASE("type deduction for function template arguments")
{
    type_deduction(7);
    type_deduction(3.14); 

    const int x = 5;
    type_deduction(&x);   
}

void foo()
{
}

TEST_CASE("auto type deduction mechanism")
{
    int x = 10;
    const int cx = 20;
    const int& ref_cx = cx;
    int* ptr = &x;

    SECTION("CASE 1 - auto without &, * modifiers")
    {
        SECTION("& and const, volatile modifiers are removed")
        {
            auto ax1 = cx; // int
            auto ax2 = ref_cx; // int            
        }

        SECTION("table and functions decay to pointer")
        {
            int tab[10];

            auto atab = tab; // int*
            auto f = foo; // void(*)()
        }
    }

    SECTION("CASE 2 - auto with & or * modifier")
    {
        SECTION("const is preserved")
        {
            auto& aref1 = cx; // const int&
            auto* aptr1 = &cx; // const int*
        }

        SECTION("table and functions with auto ref do not decay")
        {
            int tab[10];
            const int ctab[4] = { 1, 2, 3, 4 };

            auto& ref_tab = tab; // int(&)[10]
            auto& ref_ctab = ctab; // const int(&)[4]
            auto& ref_f = foo; // void(&)()

            SECTION("auto* - table still decays")
            {
                auto* ptr_tab = tab;
                static_assert(is_same<decltype(ptr_tab), int*>::value, "ERROR");
            }
        }        
    }

    SECTION("auto with &&")
    {
        // TODO
    }
}

TEST_CASE("use cases for auto")
{
    SECTION("iteration over container")
    {
        list<int> vec = { 1, 2, 3, 4, 5 };

        for(auto it = vec.begin(); it != vec.end(); ++it)
        {
            *it += 1;
            cout << *it << " ";
        }
        cout << endl;
    }

    SECTION("smart pointers")
    {
        auto sp = make_shared<string>("text");

        auto p = make_pair(1, 3.14); // pair<int, double>
    }
}

TEST_CASE("syntax of auto")
{
    auto x = 10; // int - copy syntax

    auto x1(10); // int - direct init syntax
    // int x2(10);

    int x3{10};
    //auto x4{10}; // initializer_list<int> in C++11/14
    auto x5{10}; // int - since C++17
}