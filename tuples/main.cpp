#include <iostream>
#include <numeric>
#include <string>

#include "catch.hpp"

using namespace std;

tuple<int, int, double> calc_stats(const vector<int>& data)
{
    auto min = *min_element(begin(data), end(data));
    auto max = *max_element(begin(data), end(data));
    double avg = accumulate(begin(data), end(data), 0.0) / data.size();

    return make_tuple(min, max, avg);
}

TEST_CASE("tuples")
{
    vector<int> vec = {1, 2, 3, 4, 5, 6, 7, 665, 23};

    SECTION("get")
    {
        auto stats = calc_stats(vec);

        cout << "min: " << get<0>(stats) << endl;
        cout << "max: " << get<1>(stats) << endl;
        cout << "avg: " << get<2>(stats) << endl;
    }

    SECTION("tie")
    {
        int min, max;
        double avg;

        tie(min, max, avg) = calc_stats(vec);

        cout << "min: " << min << endl;
        cout << "max: " << max << endl;
        cout << "avg: " << avg << endl;
    }

    // SECTION("since C++17")
    // {
    //     auto[min, min, avg] = calc_stats(vec);

    //     cout << "min: " << min << endl;
    //     cout << "max: " << max << endl;
    //     cout << "avg: " << avg << endl;
    // }
}

struct Person
{
    int id;
    string fn;
    string ln;

    auto tied() const
    {
        return tie(id, fn, ln);
    }

    bool operator==(const Person& other)
    {
        return tied() == other.tied();
    }

    bool operator<(const Person& other)
    {
        return tied() < other.tied();
    }
};