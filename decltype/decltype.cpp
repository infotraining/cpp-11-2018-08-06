#include "catch.hpp"

#include <map>
#include <string>

using namespace std;

TEST_CASE("decltype")
{
    map<int, string> dict = {{1, "one"}, {2, "two"}, {3, "three"}};

    auto m1 = dict;
    REQUIRE(m1.size() == 3);

    decltype(dict) m2;
    REQUIRE(m2.empty());

    decltype(m2)::value_type x;
    static_assert(is_same<decltype(x), pair<const int, string>>::value, "ERROR");
}

template <typename T>
auto multiply(const T& a, const T& b) -> decltype(a * b)
{
    return a * b;
}

struct Vector2D
{
    double x, y;
};

double operator*(const Vector2D& a, const Vector2D& b)
{
    return a.x * b.x + a.y * b.y;
}

TEST_CASE("generic multiply function")
{
    int x = 10;
    int y = 5;

    int result = multiply(x, y);
    REQUIRE(result == 50);

    double scalar = multiply(Vector2D{1, 10}, Vector2D{5, 10});
}

namespace Cpp98
{
    template <typename T>
    struct ResultForMultiplication
    {
        typedef T result_type;
    };

    template <typename T>
    typename ResultForMultiplication<T>::result_type multiply(const T& a, const T& b)
    {
        return a * b;
    }

    template <>
    struct ResultForMultiplication<Vector2D>
    {
        typedef double result_type;
    };
}

//string foo(int x)

// since C++11 we have alternative syntax for function declaration

auto foo(int x) -> string
{
    //...

    return "3.14";
}

////////////////////////////////////////////
// since C++14

auto describe(int x)
{
    if (x % 2 == 0)
        return "even"s;

    return "odd"s;
}

auto forward(const int& x)
{
    return x;
}

namespace ver1
{
    auto find_in_map(map<int, string>& m, int key)
    {
        return m.at(key);
    }
}

TEST_CASE("auto as return type - works as auto for declaration of vars")
{
    map<int, string> dict = {{1, "one"}};

    ver1::find_in_map(dict, 1) = "jeden";

    REQUIRE(dict.at(1) == "one");
}

namespace ver2
{
    decltype(auto) find_in_map(map<int, string>& m, int key)
    {
        return m.at(key);
    }
}

TEST_CASE("decltype(auto) as return type - works as decltype")
{
    map<int, string> dict = {{1, "one"}};

    ver2::find_in_map(dict, 1) = "jeden";

    REQUIRE(dict.at(1) == "jeden");
}