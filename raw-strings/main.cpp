#include <iostream>
#include <string>

#include "catch.hpp"

using namespace std;

TEST_CASE("raw-strings")
{
    SECTION("\n\t\b have special meaning in a string")
    {
        string path = "c:\\nasz katalog\\twoj katalog\\backup";
        cout << path << endl;
    }

    SECTION("C++11 - raw string for path are better")
    {
        string path = R"(c:\nasz katalog\twoj katalog\backup)";
    }

    SECTION("multiline string")
    {
        string multiline = R"(Line1
Line2
Line3)";

        cout << multiline << endl;
    }

    SECTION("const char* works with raw-string")
    {
        const char* text = R"(abc\n\b)";

        cout << text << endl;
    }

    SECTION("support for custom delimiters")
    {
        string raw = R"abc(text"(cytat)")abc";

        cout << raw << endl;
    }    
}

TEST_CASE("utf support")
{
    auto text = u"Euro: \u0024";

    cout << text << endl;
}
