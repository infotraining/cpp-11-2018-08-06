#include <iostream>
#include <string>
#include <set>

#include "catch.hpp"

using namespace std;

namespace Ver1
{
    int gen_id()
    {
        static int id = 0;
        return ++id;
    }

    class Gadget
    {
        int id_ = gen_id();
        string name_{"unknown"};
    public:
        Gadget() = default;
        virtual ~Gadget() = default;

        explicit Gadget(int id) : Gadget{id, "not-set"}
        {}

        Gadget(int id, const string& name)  
            : id_{id}, name_{name}
        {
        }

        int id() const
        {
            return id_;
        }

        string name() const
        {
            return name_;
        }

        virtual void play() const
        {
            cout << "Gagdet " <<  name_ << " is used to play..." << endl; 
        }
    };

    class SuperGadget : public Gadget
    {
    public:
        using Gadget::Gadget;

        void play() const override
        {
            cout << "SuperGadget " << name() << " used to play..." << endl;
        }
    };

    class HyperGadget : public SuperGadget
    {
    public:
        using SuperGadget::SuperGadget;

        void play() const final
        {
            cout << "HyperGadget " << name() << " used to play..." << endl;
        }
    };
}

TEST_CASE("default special functions")
{
    using namespace Ver1;

    Gadget g1{1, "ipad"};

    Gadget g2{};
    REQUIRE(g2.id() == 1);
    REQUIRE(g2.name() == "unknown"s);

    Gadget g3{};
    REQUIRE(g3.id() == 2);

    Gadget g4{665};
    REQUIRE(g4.id() == 665);
    REQUIRE(g4.name() == "not-set"s);
}

class NoCopyable
{   
public: 
    NoCopyable() = default;
    NoCopyable(const NoCopyable&) = delete;
    NoCopyable& operator=(const NoCopyable&) = delete;
};

TEST_CASE("deleted special function")
{
    NoCopyable nc;
    //NoCopyable backup = nc; // ERROR

    NoCopyable nc2;
    //nc2 = nc; // ERROR
}

void calculate(int x)
{
    cout << "calculation for " << x << endl;
}

void calculate(double) = delete;

TEST_CASE("use of deleted function to disable implicit conversion")
{
    calculate(6);
    calculate(static_cast<int>(3.14));
    calculate(static_cast<int>(5.9f));
}

namespace Cpp98
{
    class Base
    {
        int x;
        string y;
    public:
        explicit Base(int x) : x(x), y("not-set")
        {}

        Base(int x, const string& y) : x(x), y(y)
        {}
    };

    class Derived : public Base
    {
    public:
        explicit Derived(int x) : Base(x)
        {}

        Derived(int x, const string& y) : Base(x, y)
        {}
    };
}

namespace Cpp11
{
    class Base
    {
        int x;
        string y;
    public:
        explicit Base(int x) : Base{x, "not-set"}
        {}

        Base(int x, const string& y) : x{x}, y{y}
        {}
    };

    class Derived : public Base
    {
    public:
        using Base::Base; // inheritance of constructors
        
        Derived(int, const string&) = delete;
    };
}

TEST_CASE("using inherited constructors")
{
    using namespace Cpp11;

    Derived d1{1}; // OK - using inherited constructor
    //Derived d2{1, "text"s}; // ERROR - inherited but deleted
}

template <typename T>
class IndexableSet : public std::set<T>
{
public:
    using std::set<T>::set;

    const T& operator[](size_t n) const
    {
        auto it = std::next(begin(*this), n);
        return *it;
    }
};

TEST_CASE("IndexableSet")
{
    IndexableSet<int> is = { 8, 234, 12, 1243, 1, 2354, 3, 5 };

    REQUIRE(is[0] == 1);
}

TEST_CASE("overriding play function")
{
    using namespace Ver1;

    SuperGadget sg{1, "ipad pro"};
    sg.play();

    Gadget* ptr_g = &sg;
    ptr_g->play();
}