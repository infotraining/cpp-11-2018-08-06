#include <iostream>
#include <string>

#include "catch.hpp"

using namespace std;

namespace Legacy
{
    namespace Inefficient
    {
        vector<int> load_big_data_by_rvo()
        {
            return vector<int>(1'000'000);
            ; // can be optimized - RVO (Named Return Value Optimization)
        }

        vector<int> load_big_data_by_nrvo()
        {
            vector<int> data(1'000'000);
            // ...

            return data; // can be optimized - NRVO (Named Return Value Optimization)
        }

        void process_data()
        {
            vector<int> data = Inefficient::load_big_data_by_rvo(); // cc can be optimized
        }
    }

    vector<int>* load_big_data()
    {
        vector<int>* data = new vector<int>(1'000'000);

        // ...

        return data;
    }

    void process_data()
    {
        vector<int>* data = load_big_data();

        //...
    } // leak
}

string full_name(const string& first_name, const string& last_name)
{
    return first_name + " " + last_name;
}

TEST_CASE("reference binding rule")
{
    int x = 10;

    SECTION("C++98")
    {
        SECTION("l-value can be bound to l-value ref")
        {
            int& ref_x = x;
        }

        SECTION("r-value can be bound only to l-value ref to const")
        {
            const string& ref_full_name = full_name("Jan", string("Kowalski"));

            cout << "Full name: " << ref_full_name << endl;
        }
    }

    SECTION("C++11")
    {
        SECTION("r-value can bound to r-value ref")
        {
            string&& rv_ref_full_name = full_name("Jan", "Kowlski");

            cout << "Full name: " << rv_ref_full_name << endl;

            rv_ref_full_name.clear();
        }

        SECTION("l-value can not be bound to r-value ref")
        {
            //int&& rv_ref_x = x;  // ERROR
        }
    }
}

namespace MoveSemantics
{
    namespace OnlyMoveable
    {
        class Bitmap
        {
            char* image_;
            size_t size_;
            string path_;

        public:
            Bitmap(const string& path)
                : image_{new char[path.length()]}
                , size_{path.length()}
                , path_{path}
            {
                copy(begin(path_), end(path_), image_);

                cout << "Bitmap(" << path_ << ")" << endl;
            }

            Bitmap(const Bitmap&) = delete;
            Bitmap& operator=(const Bitmap&) = delete;

            // move constructor
            Bitmap(Bitmap&& source) noexcept
                : image_{source.image_}
                , size_{source.size_}
                , path_{move(source.path_)}
            {
                source.image_ = nullptr;

                cout << "Bitmap(mv: " << path_ << ")" << endl;
            }

            // move assignment operator=
            Bitmap& operator=(Bitmap&& source)
            {
                if (this != &source)
                {
                    cout << "Bitmap mv op=: ";
                    Bitmap tmp(move(source));
                    swap(tmp);
                }

                return *this;
            }

            ~Bitmap()
            {
                delete[] image_;
                cout << "~Bitmap(" << path_ << ") ";

                if (image_ == nullptr)
                    cout << " - after move";

                cout << endl;
            }

            void swap(Bitmap& other)
            {
                std::swap(image_, other.image_);
                std::swap(size_, other.size_);
                path_.swap(other.path_);
            }

            size_t size() const
            {
                return size_;
            }

            void draw() const
            {
                cout << "Drawing: ";

                for (size_t i = 0; i < size_; ++i)
                    cout << image_[i];
                cout << endl;
            }
        };
    }

    inline namespace CopyableAndMoveable
    {
        class Bitmap
        {
            char* image_;
            size_t size_;
            string path_;

        public:
            Bitmap(const string& path)
                : image_{new char[path.length()]}
                , size_{path.length()}
                , path_{path}
            {
                copy(begin(path_), end(path_), image_);

                cout << "Bitmap(" << path_ << ")" << endl;
            }

            Bitmap(const Bitmap& source)
                : image_{new char[source.size_]}
                , size_{source.size_}
                , path_{source.path_}
            {
                copy(source.image_, source.image_ + size_, image_);
                cout << "Bitmap(cc: " << path_ << ")" << endl;
            }

            Bitmap& operator=(const Bitmap& source)
            {
                cout << "Bitmap cc op=: ";
                Bitmap tmp{source};
                swap(tmp);

                return *this;
            }

            // move constructor
            Bitmap(Bitmap&& source) noexcept
                : image_{source.image_}
                , size_{source.size_}
                , path_{move(source.path_)}
            {
                source.image_ = nullptr;

                cout << "Bitmap(mv: " << path_ << ")" << endl;
            }

            // move assignment operator
            Bitmap& operator=(Bitmap&& source)
            {
                if (this != &source)
                {
                    cout << "Bitmap mv op=: ";
                    Bitmap tmp(move(source));
                    swap(tmp);
                }

                return *this;
            }

            ~Bitmap()
            {
                delete[] image_;
                cout << "~Bitmap(" << path_ << ") ";

                if (image_ == nullptr)
                    cout << " - after move";

                cout << endl;
            }

            void swap(Bitmap& other)
            {
                std::swap(image_, other.image_);
                std::swap(size_, other.size_);
                path_.swap(other.path_);
            }

            size_t size() const
            {
                return size_;
            }

            void draw() const
            {
                cout << "Drawing: ";

                for (size_t i = 0; i < size_; ++i)
                    cout << image_[i];
                cout << endl;
            }
        };
    }

    namespace OptimizedImpl
    {
        class Bitmap
        {
            vector<char> image_;            
            string path_;

        public:
            Bitmap(const string& path)
                : image_(path.length())                
                , path_{path}
            {
                copy(begin(path_), end(path_), begin(image_));

                cout << "Bitmap(" << path_ << ")" << endl;
            }                        

            void swap(Bitmap& other)
            {
                std::swap(image_, other.image_);
                path_.swap(other.path_);
            }

            size_t size() const
            {
                return image_.size();
            }

            void draw() const
            {
                cout << "Drawing: ";

                for (const auto& b : image_)
                    cout << b;
                cout << endl;
            }
        };
    }

    Bitmap load_bitmap(const string& path)
    {
        return Bitmap{path};
    }
}

TEST_CASE("using Bitmap")
{
    using namespace MoveSemantics;

    Bitmap bmp{"landscape.bmp"};

    Bitmap new_bmp = move(bmp);
    new_bmp.draw();

    Bitmap portrait{"portrait.bmp"};
    portrait.draw();

    new_bmp = move(portrait);

    Bitmap bmp1 = load_bitmap("bmp1");
    Bitmap bmp2 = load_bitmap("bmp2");
    new_bmp = load_bitmap("bmp3");

    cout << "\n-------------------------------------------\n";
    cout << "\n-------------------------------------------\n";

    vector<Bitmap> drawings;
    //drawings.reserve(8);

    drawings.push_back(move(bmp1));
    drawings.push_back(move(bmp2));
    drawings.push_back(move(new_bmp));
    drawings.push_back(load_bitmap("bmp4"));

    for (const auto& bmp : drawings)
        bmp.draw();
}

namespace MoveSemantics
{
    namespace ImplicitSpecialFunctions
    {
        class Sprite
        {
            MoveSemantics::Bitmap bmp_;

        public:
            Sprite(MoveSemantics::Bitmap bmp)
                : bmp_{move(bmp)}
            {
            }

            void render() const
            {
                bmp_.draw();
            }
        };
    }

    inline namespace UserDeclaredSpecialFunctions
    {
        class Sprite
        {
            MoveSemantics::Bitmap bmp_;

        public:
            Sprite(MoveSemantics::Bitmap bmp)
                : bmp_{move(bmp)}
            {
            }

            Sprite(const Sprite&) = default;
            Sprite& operator=(const Sprite&) = default;
            Sprite(Sprite&&) = default;
            Sprite& operator=(Sprite&&) = default;

            virtual ~Sprite() = default;

            void render() const
            {
                bmp_.draw();
            }
        };
    }
}

TEST_CASE("passing args in modern way")
{
    cout << "\n---------------------------\n";

    using namespace MoveSemantics;

    Sprite sprite{load_bitmap("sprite1")};
    sprite.render();

    vector<Sprite> sprites;
    sprites.push_back(move(sprite));
}

void foo(string&& text)
{    
}

template <typename T>
void generic_foo(T&& item) // universal reference
{
}

TEST_CASE("reference collapsing")
{
    auto&& ax = 4;  // int&& - because r-value
    
    int x = 665;
    auto&& bx = x; // int& - because l-value

    vector<bool> vec_bool = { 1, 0, 0, 0, 1 };

    for(auto&& flag : vec_bool)
        flag.flip();

    for(const auto& flag : vec_bool)
        cout << flag << " ";
    cout << endl;
}

template <typename T>
struct Container
{
    T items[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

    struct ItemProxy
    {
        T* item;

        void negate()
        {
            *item = -*item;
        }

        T value() const
        {
            return *item;
        }
    };

public:
    ItemProxy operator[](size_t index)
    {
        return { items + index };
    }
};

TEST_CASE("using container with proxy")
{
    Container<int> c;

    auto&& item = c[5];

    cout << item.value() << endl;
    item.negate();

    cout << c[5].value() << endl;

    // for(auto&& item : c)
    //     item.negate();
}

////////////////////////////////////////////////////////
////////////////////////////////////////////////////////

// move-only type
template <typename T>
class UniquePtr
{
    T* ptr_;

public:
    explicit UniquePtr(T* ptr)
        : ptr_{ptr}
    {
    }

    UniquePtr(const UniquePtr&) = delete;
    UniquePtr& operator=(const UniquePtr&) = delete;

    UniquePtr(UniquePtr&& source) noexcept
        : ptr_{source.ptr_}
    {
        source.ptr_ = nullptr;
    }

    UniquePtr& operator=(UniquePtr&& source) noexcept
    {
        if (this != source)
        {
            delete ptr_;
            ptr_        = source.ptr_;
            source.ptr_ = nullptr;
        }

        return *this;
    }

    ~UniquePtr()
    {
        delete ptr_;
    }

    T* get() const
    {
        return ptr_;
    }

    T& operator*() const
    {
        return *ptr_;
    }

    T* operator->() const
    {
        return ptr_;
    }

    explicit operator bool()
    {
        return ptr_ != nullptr;
    }
};

template <typename T, typename Arg>
UniquePtr<T> create_unique(Arg&& arg)
{
    return UniquePtr<T>{ new T(std::forward<Arg>(arg)); }
}

TEST_CASE("unique_ptr")
{
    unique_ptr<string> uptr = make_unique<string>("text");

    cout << *uptr << " - size: " << uptr->size() << endl;

    unique_ptr<string> other = move(uptr);

    REQUIRE(uptr.get() == nullptr);

    cout << *other << endl;
}   

namespace LegacyCode
{
    using namespace MoveSemantics;

    Bitmap* create_bitmap(const string& path)
    {
        return new Bitmap(path);
    }

    // Bitmap* create_bitmaps(size_t size)
    // {
    //     return new Bitmap[size];
    // }

    void use(Bitmap* bmp)
    {
        if (bmp)
            bmp->draw();
    }

    void play(Bitmap* bmp)
    {
        if (bmp)
            bmp->draw();

        delete bmp;
    }

    void leaky_code()
    {
        create_bitmap("bmp1"); // leak

        LegacyCode::use(create_bitmap("bmp2")); // leak

        Bitmap* bmp = create_bitmap("bmp3");

        LegacyCode::use(bmp); // ok
        play(bmp);

        bmp->draw(); // segfault
    }
}

namespace ModernCpp
{
    using namespace MoveSemantics;

    unique_ptr<Bitmap> create_bitmap(const string& path)
    {
        return make_unique<Bitmap>(path);
    }

    vector<Bitmap> create_bitmaps()
    {
        vector<Bitmap> local_vec{ Bitmap{"1.bmp"}, Bitmap{"2.png"} };

        return local_vec;
    }

    void use(Bitmap* bmp)
    {
        if (bmp)
            bmp->draw();
    }

    void play(unique_ptr<Bitmap> bmp)
    {
        if (bmp)
            bmp->draw();        
    }    

    void safe_code()
    {
        create_bitmap("bmp1"); // ok

        vector<Bitmap> bmps = create_bitmaps();

        auto bmp2 = create_bitmap("bmp2");
        LegacyCode::use(bmp2.get()); // ok

        unique_ptr<Bitmap> bmp3 = create_bitmap("bmp3");

        LegacyCode::use(bmp3.get()); // ok
        play(move(bmp3));        

        unique_ptr<Bitmap> lbmp{ LegacyCode::create_bitmap("jpg1") };
    }
}

namespace CCode
{
    void* reserve_buffer(unsigned int size)
    {
        void* raw_mem = malloc(size);
        memset(raw_mem, 0, size);

        cout << "memory allocated at " << raw_mem << endl;

        return raw_mem;
    }

    void free_buffer(void* buffer)
    {
        cout << "memory at " << buffer << " freed" << endl;

        free(buffer);
    }
}

struct DeallocByFree
{
    void operator()(void* mem) const
    {
        cout << "DeallocByFree memory at " << mem << " freed" << endl;
        free(mem);
    }
};

using CPtr = unique_ptr<void, DeallocByFree>;

CPtr create_safe_wrapper(void* mem)
{
    return CPtr{mem, DeallocByFree{}};
}

TEST_CASE("coop with c-code")
{
    using namespace CCode;    

    SECTION("auto cleanup using custom dealloc")
    {
        unique_ptr<void, DeallocByFree> mem{reserve_buffer(1024), DeallocByFree{}};
        cout << "Using mem at " << mem.get() << endl;

        auto safe_mem = create_safe_wrapper(reserve_buffer(2048));
        //use(safe_mem.get());
    }

    cout << "\n\n";

    SECTION("auto cleanup using custom dealloc")
    {
        unique_ptr<void, DeallocByFree> mem{reserve_buffer(1024), DeallocByFree{}};
        
        cout << "Using mem at " << mem.get() << endl;

        free_buffer(mem.release());
    }
}