#include <iostream>
#include <string>
#include <algorithm>
#include <numeric>
#include <queue>
#include <future>
#include <thread>

#include "catch.hpp"

using namespace std;

class Lambda_27482713517835
{
public:
    void operator()() const { cout << "Hello from closure class" << endl; }
};

TEST_CASE("lambda")
{
    auto lambda = []{ cout << "Hello from lambda" << endl; };

    lambda();

    SECTION("is interpreted as")
    {
        auto lambda = Lambda_27482713517835{};

        lambda();
    }
}

TEST_CASE("lambda with args")
{
    auto add = [](int a, int b) { return a + b; };

    cout << add(4, 5) << endl;
}

template <typename Iter, typename F>
void for_each_n(Iter it, size_t n, F f)
{
    using value_type = decltype(*it);
    static_assert(is_same<decltype(f(declval<value_type>())), void>::value, "Function must return void");

    for(size_t i = 0; i < n; ++i, ++it)
    {
        f(*it);
    }
}

TEST_CASE("passing lambda as argument")
{
    vector<int> vec(10);
    iota(vec.begin(), vec.end(), 1);

    for_each_n(vec.begin(), 5, [](int x) { cout << x << " "; });
    cout << endl;
}

TEST_CASE("use case for lambda")
{
    vector<int> vec = { 1, 6347, 23, 645, 234, 4632, 234365, 234 } ;

    auto pos_even = find_if(begin(vec), end(vec), [](int x) { return x % 2 == 0; });

    if (pos_even != end(vec))
        cout << *pos_even << " found" << endl;
}

bool compare(int a, int b)
{
    return a > b;
}

TEST_CASE("storing lambdas")
{
    vector<int> vec = { 1, 6347, 23, 645, 234, 4632, 234365, 234 } ;

    SECTION("auto var - best way")
    {
        auto comp_desc = [](int a, int b) {  return a > b; };

        sort(begin(vec), end(vec), comp_desc);

        for(const auto& item : vec)
            cout << item << " ";
        cout << endl;
    }

    SECTION("std::function")
    {
        function<bool(int, int)> comp_desc;
        comp_desc = &compare;
        comp_desc = [](int a, int b) {  return a > b; };

        sort(begin(vec), end(vec), comp_desc);

        for(const auto& item : vec)
            cout << item << " ";
        cout << endl;
    }
}

TEST_CASE("queue of jobs")
{
    using Task = function<void()>;
    queue<Task> tasks;

    tasks.push([] { cout << "Start" << endl; });
    tasks.push([] { cout << "Running" << endl; });
    tasks.push([] { cout << "End" << endl; });

    ////////

    while(!tasks.empty())
    {
        Task task = tasks.front();
        task();
        tasks.pop();
    }
}

class Lambda_2346273643176
{
    const int tshd_;
public:
    Lambda_2346273643176(int t) : tshd_{t} {}

    auto operator()(int x) { return x > tshd_; }
};

TEST_CASE("capturing variables")
{
    vector<int> vec = { 234, 34, 134, 4235, 5235, 2355 };

    SECTION("by value")
    {
        int threshold = 665;

        auto pred = [threshold](int x) { return x > threshold; };

        threshold = 6666;

        auto pos = find_if(begin(vec), end(vec), pred);

        if (pos != end(vec))
            cout << "Found: " << *pos << endl;
    }

    SECTION("by ref")
    {
        int sum{};

        for_each_n(begin(vec), 4, [&sum](int x) { sum += x; });

        cout << "sum: " << sum << endl;
    }

    SECTION("by move")
    {
        unique_ptr<string> ptr = make_unique<string>("text");

        auto lambda_ms = [mptr = move(ptr)] { cout << *mptr << endl; };

        lambda_ms();
    }
}

class Data
{
    vector<int> data_ = { 2342, 52, 23, 25534, 2325, 53452, 345 };
    int threshold_ = 665;

public:
    int find_item()
    {
        return *find_if(begin(data_), end(data_), [this](int x) { return x > threshold_; });
    }
};


class Lambda_2384728936923
{
public:
    template <typename T>
    auto operator()(const T& item) const { cout << "item: " << item << endl; };
};

TEST_CASE("generic lambda")
{
    auto print = [](const auto& item) { cout << "item: " << item << endl; };

    print(1);
    print("text");
    print(3.14);
    print("string"s);
}

/////////////////////////////////////////////////////////
//

string download_file(const string& url)
{
    cout << "Start download: " << url << endl;

    this_thread::sleep_for(chrono::milliseconds(250 * url.size()));

    cout << "End of download: " << url << endl;
    
    return "Content of "s + url;
}

void save_to_file(const string& file_name, const string& content)
{
    cout << "Save to file: " << file_name << endl;

    this_thread::sleep_for(chrono::milliseconds(500 * file_name.size()));

    cout << "File with " << content << " saved: " << endl;
}

void process_file(shared_future<string> content)
{
    cout << "Processing file: " << content.get() << endl;
}

TEST_CASE("async functions")
{
    future<string> f_content1 = async(launch::async, download_file, "google.com");
    future<string> f_content2 = async(launch::async, [] { return download_file("infotraining.pl"); });

    cout << "Main thread still works" << endl;

    const string content1 = f_content1.get();
    shared_future<string> sf_content2 = f_content2.share();

    auto _0 = async(launch::async, [sf_content2]{ process_file(sf_content2) ; });

    auto _1 = async(launch::async, [&] { save_to_file("file1.txt", content1); }); 
    auto _2 = async(launch::async, [&] { save_to_file("file2.txt", sf_content2.get()); }); 

    cout << "content1: " << content1 << endl;
    cout << "content2: " << sf_content2.get() << endl;
}