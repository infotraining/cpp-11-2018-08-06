#include <iostream>
#include <string>

#include "catch.hpp"

using namespace std;

TEST_CASE("range-based-for")
{
    vector<unsigned int> vec = {1, 2, 3, 4};

    for (int item : vec)
    {
        cout << item << " ";
    }
    cout << endl;

    SECTION("is interpreted")
    {
        SECTION("case 1")
        {
            for (auto it = vec.begin(); it != vec.end(); ++it)
            {
                int item = *it;
                cout << item << " ";
            }
        }

        SECTION("case 2 - native array")
        {
            int tab[5] = { 534, 234, 5, 23, 534 };

            for(auto item : tab)
            {
                cout << item << " ";
            }
            cout << endl;

            for (auto it = begin(tab); it != end(tab); ++it)
            {
                int item = *it;
                cout << item << " ";
            }
            cout << endl;
        }
    }

    SECTION("works with initializer_list")
    {
        for(int i : { 1, 2, 3 })
        {
            cout << i << " ";
        }
        cout << endl;
    }
}

TEST_CASE("efficient range-based-for")
{
    vector<string> words = { "one", "two", "three" };

    for(auto& w : words)
        w += w;

    for(const auto& w : words)
    {
        cout << w << " ";
    }        
}