#include <iostream>
#include <string>
#include <map>

#include "catch.hpp"

using namespace std;

enum Coffee : uint8_t { espresso, latte, cappucino };

TEST_CASE("enum can be defined on any integral type")
{
    static_assert(sizeof(Coffee) == 1);
}

enum class Engine : uint8_t { diesel = 1, petrol, electric };

TEST_CASE("enum vs enum_class")
{
    SECTION("enum_class restricts names to scope")
    {
        Coffee c = espresso;

        Engine e = Engine::electric;
    }

    SECTION("implicit conversion from/to integral type is an error")
    {
        Coffee c = latte;
        int value_c = c;

        Engine e = Engine::petrol;
        int value_e = static_cast<int>(e);
        REQUIRE(value_e == 2);

        e = static_cast<Engine>(1);
        REQUIRE(e == Engine::diesel);
    }
}

template <typename T>
using Dictionary = map<string, T>;

TEST_CASE("aliases for types")
{
    typedef const int& reference_int;

    using const_reference_double = const double&;    

    Dictionary<int> dict = { { "one", 1 }, { "two", 2 } };
    REQUIRE(dict.at("one") == 1);
}