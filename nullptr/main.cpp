#include <iostream>
#include <string>

#include "catch.hpp"

using namespace std;

TEST_CASE("nullptr is better NULL")
{
    int* ptr = nullptr; // setting 0 to ptr

    REQUIRE(ptr == 0);
    REQUIRE(ptr == nullptr);
}

void foo(int* ptr)
{
    cout << "foo(int*: ";

    if (ptr)
        cout << *foo;    
    else    
        cout << "nullptr";    

    cout << ")\n";
}

void foo(long n)
{
    cout << "foo(long: " << n << ")" << endl;
}

void foo(nullptr_t)
{
    cout << "foo(nullptr)" << endl;
}

TEST_CASE("why nullptr is better than NULL")
{
    //foo(NULL); // ambigous call

    int x = 10;

    foo(nullptr);
    foo(&x);
    foo(0L);

    int* ptr = nullptr;
    foo(ptr);
}   