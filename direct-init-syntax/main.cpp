#include <iostream>
#include <map>
#include <string>

#include "catch.hpp"

using namespace std;

struct X
{
    int a;
    double b;
};

TEST_CASE("uniform init syntax")
{
    SECTION("C++98")
    {
        int x1;
        int x2 = 10;
        int x3(10);
        int x4(); // most vexing parse - may cause problems
        X x5{1, 3.14}; //aggregate
        int tab[] = {1, 2, 3, 4}; // native array is aggregate
        pair<int, double> p(1, 3.14);
        vector<int> vec;
        vec.push_back(1);
        vec.push_back(2);
        vec.push_back(3);
    }

    SECTION("C++11")
    {
        int x1;
        int x2 = 10;
        int x3{10};
        int x4{};
        X x5{1, 3.14}; //aggregate
        int tab[] = {1, 2, 3, 4}; // native array is aggregate
        pair<int, double> p{1, 3.14};
        vector<int> vec1 = {1, 2, 3, 4, 5};
        vector<int> vec2{1, 2, 3, 4, 5};
    }

    SECTION("narrowing conversion is disallowed")
    {
        int x = 665;

        uint8_t sx1 = x;
        //uint8_t sx2{x}; // Compilation ERROR - narrowing conversion
    }
}

TEST_CASE("initializer_list")
{
    initializer_list<int> il = {1, 2, 4, 5, 6};

    REQUIRE(il.size() == 6);
    REQUIRE(*il.begin() == 1);
}

struct Data
{
    vector<int> values;

    Data(initializer_list<int> il) : values{il}
    {
    }
};

TEST_CASE("init from list")
{
    Data data = {1, 2, 3, 4, 5};

    REQUIRE(data.values.size() == 5);

    for (const auto& item : data.values)
        cout << item << " ";
    cout << endl;
}

TEST_CASE("auto with initializer_list")
{
    auto lst = {1, 2, 3, 4};
    static_assert(is_same<decltype(lst), initializer_list<int>>::value, "ERROR");

    //auto empty = {}; // initializer_list<int>

    //auto lst2{10, 20, 30}; // since C++17 is an error
}

TEST_CASE("one problem with initializer list")
{
    vector<int> v1(4, 1); // [1, 1, 1, 1]

    vector<int> v2{10, 1}; // [10, 1]
}